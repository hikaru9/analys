CC := gcc
CFLAGS := 
LDLIBS := -lm

TARGET := main
OBJECTS := main.o calc_osc.o i-v_char.o parameter_parser.o

C_DEBUG_FLAGS := -Wall -Wextra -g3

.PHONY: debug
debug: CFLAGS += $(C_DEBUG_FLAGS)
debug: all

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJECTS)

main.o: calc_osc.h I-V_char/i-v_char.h I-V_char/parameter_parser.h 

parameter_parser.o: I-V_char/parameter_parser.c I-V_char/i-v_char.h
	$(CC) $(CFLAGS) $(LDLIBS) I-V_char/parameter_parser.c -o parameter_parser.o -c

i-v_char.o: I-V_char/i-v_char.h
	$(CC) $(CFLAGS) $(LDLIBS) I-V_char/i-v_char.c -o i-v_char.o -c

calc_osc.o: calc_osc.h

.PHONY: clean
clean:
	$(RM) *.o
	$(RM) main

.PHONY: run
run: main
	./main
