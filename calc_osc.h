/*
  calc_osc.h
*/

#ifndef _CALC_OSC_H
#define _CALC_OSC_H

typedef struct
{
  double V_b;
  double R_b;
  double R_L;
  double L_E;
  double R_rad;
  double C_rad;
	double R_a;
  double C_RTD;
} CircuitParameter;

typedef struct
{
  double i_E;
  double v_RTD;
  double v_crad;
} CircuitVariable;

CircuitVariable runge_kutta(double dt, CircuitVariable x, CircuitParameter param, TBRTDproperties prp);
#endif
