#!/bin/bash

#
IFS=$','

while read LINE; do
	csvList=($(echo "$LINE"))
	baseDir="./result/LE${csvList[0]}RL${csvList[1]}"
	#echo $baseDir
	if [ ! -e $baseDir ]; then
		mkdir $baseDir
	fi
	if [ -e "./run_OSC.exe" ]; then
		./main.exe ${csvList[0]} ${csvList[1]} ${baseDir}
	else
		./main ${csvList[0]} ${csvList[1]} ${baseDir}
	fi
	#echo "./run_OSC" "\"${baseDir}/\"" "${csvList[0]}" "${csvList[1]}" "${csvList[2]}"
done < $1

