/*
 * Title: Circuit Transient Analysis
 * Description: Bow-Tie Anttena Integrated RTD Oscirator Equivalent Circuit Transient Analysis
 * Note: p.36-39
 * Writer: pochiMasahiro
 * Date: 2016/08/12
 */

#include "I-V_char/i-v_char.h"
#include "I-V_char/parameter_parser.h"
#include "calc_osc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argv, char *args[])
{
	/* shell argument check */
	//if(argv != 4) exit(1);

	/* RTD and Circuit parameter variable definition */
  TBRTDproperties PRP = parameter();
  CircuitVariable x = {0.0, 0.0, 0.0};
  CircuitParameter param;

	/* Output settings */
	/*char base[100];
	char log[] = "./result/log.txt";
	FILE *fp_res, *fp_log;
	strcpy(base, args[3]);
	strcat(base, "/_x.tsv");
	fp_res = fopen(base, "w");
	fp_log = fopen(log, "a+");*/
	
	/* Get execusion time */
	time_t tm = time(NULL);
	char date[64];
	strftime(date, sizeof(date), "%Y/%m/%d %a %H:%M:%S", localtime(&tm));

	/* Circuit parameter definition */
  param.R_b = 50.0;
  param.R_L = 20.0;
  param.L_E = 10e-10; 
  param.R_a = 2.0 * 15.0;
  param.C_rad = 0.8e-15;
  param.C_RTD = 11.25e-15;

  // DC analysis
  double v_rtd = 0.6;
  double i_rtd_dc = j_tbrtd(v_rtd, PRP);
  param.V_b = param.R_b * i_rtd_dc + (param.R_b / param.R_L + 1) * (v_rtd + i_rtd_dc * param.R_a);

	/* settings runge-kutta method */
	double t = 0;
  double h = (160e-12 - 0) / 10000;

	/* Print setteings*/
	/*fprintf(stdout, "---------------------------------------------------------------------------------\n");
	fprintf(stdout, "Directry: %s\n", base);
	fprintf(stdout, "Parameter:\nR_b = %E[ohm], R_L = %E[ohm], L_c = %E[H], L_E = %E[H],\nR_ant = %E[ohm], L_ant = %E[H], R_rad = %E[ohm],\nC_rad = %E[F], C_RTD = %E[F], V_b = %E[V]\n", param.R_b, param.R_L, param.L_c, param.L_E, param.R_ant, param.L_ant, param.R_rad, param.C_rad, param.C_RTD, param.V_b);
	fprintf(stdout, "---------------------------------------------------------------------------------\n");*/
  
	/* execusion runge-kutta method */
  for (t = 0; t < 160e-12; t += h) {
    fprintf(stdout, "%E %E %E %E \n", t, x.i_E, x.v_RTD, x.v_crad);
    x = runge_kutta(h, x, param, PRP);
  }

	/* generation of Log file */
	/*fprintf(fp_log, "---------------------------------------------------------------------------------\n");
	fprintf(fp_log, "Directry: %s\n", base);
	fprintf(fp_log, "Date: %s\n", date);
	fprintf(fp_log, "Parameter:\nR_b = %E[ohm], R_L = %E[ohm], L_c = %E[H], L_E = %E[H],\nR_ant = %E[ohm], L_ant = %E[H], R_rad = %E[ohm],\nC_rad = %E[F], C_RTD = %E[F], V_b = %E[V]\n", param.R_b, param.R_L, param.L_c, param.L_E, param.R_ant, param.L_ant, param.R_rad, param.C_rad, param.C_RTD, param.V_b);
	fprintf(fp_log, "---------------------------------------------------------------------------------\n");*/
	
	/* File close */
	//fclose(fp_log);
	//fclose(fp_res);
  return 0;
}
