#include "I-V_char/i-v_char.h"
#include "calc_osc.h"

CircuitVariable calc_funct(CircuitVariable x, CircuitParameter param, TBRTDproperties prp)
{
  CircuitVariable xr;
	double v_in = 0.0;
  xr.i_E = (v_in - x.v_crad + (param.V_b - param.R_L*x.i_E) / (param.R_b + param.R_L) * param.R_b - param.V_b) / param.L_E;
  xr.v_crad = (x.i_E + (v_in - x.v_crad - x.v_RTD) / param.R_a) / param.C_rad;
  xr.v_RTD = ((v_in - x.v_crad - x.v_RTD) / param.R_a - j_tbrtd(x.v_RTD, prp)) / param.C_RTD;
  return xr;
}

CircuitVariable calc_next(CircuitVariable x, CircuitVariable xk, double dt)
{
  CircuitVariable xr;
  xr.i_E = x.i_E + xk.i_E * dt;
  xr.v_RTD = x.v_RTD + xk.v_RTD * dt;
  xr.v_crad = x.v_crad + xk.v_crad * dt;
  return xr;
}

CircuitVariable calc_last(double dt, CircuitVariable x[], CircuitVariable xr)
{
  CircuitVariable xt;
  xt.i_E = xr.i_E + (x[0].i_E + 2.0 * x[1].i_E + 2.0 * x[2].i_E + x[3].i_E)*dt/6.0;
  xt.v_RTD = xr.v_RTD + (x[0].v_RTD + 2.0 * x[1].v_RTD + 2.0 * x[2].v_RTD + x[3].v_RTD)*dt/6.0;
  xt.v_crad = xr.v_crad + (x[0].v_crad + 2.0 * x[1].v_crad + 2.0 * x[2].v_crad + x[3].v_crad)*dt/6.0;
  return xt;  
}

CircuitVariable runge_kutta(double dt, CircuitVariable x, CircuitParameter param, TBRTDproperties prp)
{
  CircuitVariable xk[4], xt;
  xk[0] = calc_funct(x, param, prp);
  xt = calc_next(x, xk[0], dt/2);
  xk[1] = calc_funct(xt, param, prp);
  xt = calc_next(x, xk[1], dt/2);
  xk[2] = calc_funct(xt, param, prp);
	xt = calc_next(x, xk[2], dt);
  xk[3] = calc_funct(xt, param, prp);
  return calc_last(dt, xk, x);
}
